#!/usr/bin/env python



import os
#import json
import sys
import time
import logging
import platform
import _thread as thread   # for daemon = True  / Python 3.x
import configparser
import paho.mqtt.client as mqtt

from gi.repository import GLib as gobject  # Python 3.x

# our own packages
#sys.path.insert(1, os.path.join(os.path.dirname(__file__), '../ext/velib_python'))
sys.path.insert(1, os.path.join(os.path.dirname(__file__), '/opt/victronenergy/dbus-systemcalc-py/ext/velib_python'))
from vedbus import VeDbusService
PATH_UPDATEINDEX = '/UpdateIndex'

# get values from config.ini file
try:
    config = configparser.ConfigParser()
    config.read("%s/config.ini" % (os.path.dirname(os.path.realpath(__file__))))
    if (config['MQTT']['broker_address'] == "IP_ADDR_OR_FQDN"):
        print("ERROR:config.ini file is using invalid default values like IP_ADDR_OR_FQDN. The driver restarts in 60 seconds.")
        time.sleep(60)
        sys.exit()
except:
    print("ERROR:config.ini file not found. Copy or rename the config.sample.ini to config.ini. The driver restarts in 60 seconds.")
    time.sleep(60)
    sys.exit()


# Get logging level from config.ini
# ERROR = shows errors only
# WARNING = shows ERROR and warnings
# INFO = shows WARNING and running functions
# DEBUG = shows INFO and data/values
if 'DEFAULT' in config and 'logging' in config['DEFAULT']:
    if config['DEFAULT']['logging'] == 'DEBUG':
        logging.basicConfig(level=logging.DEBUG)
    elif config['DEFAULT']['logging'] == 'INFO':
        logging.basicConfig(level=logging.INFO)
    elif config['DEFAULT']['logging'] == 'ERROR':
        logging.basicConfig(level=logging.ERROR)
    else:
        logging.basicConfig(level=logging.WARNING)
else:
    logging.basicConfig(level=logging.WARNING)
    

# Default vars
is_connected = 0
powercurr = 0
totalin = 0
totalout = 0
voltage = 0
current = 0


def on_disconnect(client, userdata, rc):
    global is_connected
    print("Client Got Disconnected")
    if rc != 0:
        print('Unexpected MQTT disconnection. Will auto-reconnect')

    else:
        print('rc value:' + str(rc))

    try:
        print("Trying to Reconnect")
        client.connect(config['MQTT']['broker_address'])
        is_connected = 1
    except Exception as e:
        logging.exception("Error in reconnect with broker")
        print("Error in Retrying to Connect with Broker")
        is_connected = 0
        print(e)


def on_connect(client, userdata, flags, rc):
    global is_connected
    if rc == 0:
        print("Connected to MQTT Broker!")
        is_connected = 1
        print("subscribing to:" + config['MQTT']['topic'])
        client.subscribe(config['MQTT']['topic'] + "/#")
    else:
        print("Failed to connect, return code %d\n", rc)


def on_message(client, userdata, msg):
    try:
        global powercurr, totalin, totalout, voltage, current
        #print(f"Message received [{msg.topic}]: {msg.payload}")
        if msg.payload != '{"value": null}' and msg.payload != b'{"value": null}':
            if msg.topic == config['MQTT']['topic'] + "/power":
                powercurr = float(msg.payload)
            if msg.topic == config['MQTT']['topic'] + "/total":
                totalin = float(msg.payload)
            if msg.topic == config['MQTT']['topic'] + "/total_returned":
                totalout = float(msg.payload)
            if msg.topic == config['MQTT']['topic'] + "/voltage":
                voltage = float(msg.payload)
            if msg.topic == config['MQTT']['topic'] + "/current":
                current = float(msg.payload)
        else:
            print("Ignored null response from broker.")

    except Exception as e:
        logging.exception("Program MQTTtoMeter crashed. (on message function)")
        print(e)
        print("Error in on_message function")


class DbusDummyService:
    def __init__(self, servicename, deviceinstance, paths, productname='MQTT-EM3', connection='MQTT'):
        self._dbusservice = VeDbusService(servicename)
        self._paths = paths

        logging.debug("%s /DeviceInstance = %d" %
                      (servicename, deviceinstance))

        # Create the management objects, as specified in the ccgx dbus-api document
        self._dbusservice.add_path('/Mgmt/ProcessName', __file__)
        self._dbusservice.add_path(
            '/Mgmt/ProcessVersion', 'Unkown version, and running on Python ' + platform.python_version())
        self._dbusservice.add_path('/Mgmt/Connection', connection)

        # Create the mandatory objects
        self._dbusservice.add_path('/DeviceInstance', deviceinstance)
        # value used in ac_sensor_bridge.cpp of dbus-cgwacs
        self._dbusservice.add_path('/ProductId', 45069)
        self._dbusservice.add_path('/ProductName', productname)
        self._dbusservice.add_path('/CustomName', config['DEFAULT']['device_name'])
        self._dbusservice.add_path('/FirmwareVersion', 0.1)
        self._dbusservice.add_path('/HardwareVersion', 0)
        self._dbusservice.add_path('/Connected', 1)
        self._dbusservice.add_path('/AllowedRoles', ['grid', 'pvinverter', 'genset', 'acload'])

        for path, settings in self._paths.items():
            self._dbusservice.add_path(
                path, settings['initial'], writeable=True, onchangecallback=self._handlechangedvalue)

        # pause 1000ms before the next request
        gobject.timeout_add(1000, self._update)

    #
    # Only values from a single phase (L1) is returned to dbus.
    # Other values are hardcoded 0 so L2 & L3 of the EM3 can be user for something else
    #
    def _update(self):
        # positive: consumption, negative: feed into grid
        self._dbusservice['/Ac/Power'] = powercurr
        self._dbusservice['/Ac/L1/Voltage'] = round(voltage, 2)
        #self._dbusservice['/Ac/L2/Voltage'] = 0
        #self._dbusservice['/Ac/L3/Voltage'] = 0
        self._dbusservice['/Ac/L1/Current'] = round(current, 2)
        #self._dbusservice['/Ac/L2/Current'] = 0
        #self._dbusservice['/Ac/L3/Current'] = 0
        self._dbusservice['/Ac/L1/Power'] = round(powercurr, 2)
        #self._dbusservice['/Ac/L2/Power'] = 0
        #self._dbusservice['/Ac/L3/Power'] = 0
        self._dbusservice['/Ac/Energy/Forward'] = totalin
        self._dbusservice['/Ac/Energy/Reverse'] = totalout
        self._dbusservice['/Ac/L1/Energy/Forward'] = totalin
        self._dbusservice['/Ac/L1/Energy/Reverse'] = totalout

        logging.info("House Consumption: {:.0f}".format(powercurr))
        # increment UpdateIndex - to show that new data is available
        index = self._dbusservice[PATH_UPDATEINDEX] + 1  # increment index
        if index > 255:   # maximum value of the index
            index = 0       # overflow from 255 to 0
        self._dbusservice[PATH_UPDATEINDEX] = index
        return True

    def _handlechangedvalue(self, path, value):
        logging.debug("someone else updated %s to %s" % (path, value))
        return True  # accept the change


def main():
    # use .INFO for less logging or NOTSET
    #logging.basicConfig(level=logging.INFO)
    thread.daemon = True  # allow the program to quit

    from dbus.mainloop.glib import DBusGMainLoop
    # Have a mainloop, so we can send/receive asynchronous calls to and from dbus
    DBusGMainLoop(set_as_default=True)

    pvac_output = DbusDummyService(
        servicename='com.victronenergy.grid.cgwacs_ttyUSB0_mb1',
        deviceinstance=0,
        paths={
            '/Ac/Power': {'initial': 0},
            '/Ac/L1/Voltage': {'initial': 0},
            #'/Ac/L2/Voltage': {'initial': 0},
            #'/Ac/L3/Voltage': {'initial': 0},
            '/Ac/L1/Current': {'initial': 0},
            #'/Ac/L2/Current': {'initial': 0},
            #'/Ac/L3/Current': {'initial': 0},
            '/Ac/L1/Power': {'initial': 0},
            #'/Ac/L2/Power': {'initial': 0},
            #'/Ac/L3/Power': {'initial': 0},
            # energy bought from the grid
            '/Ac/Energy/Forward': {'initial': 0},
            '/Ac/Energy/Reverse': {'initial': 0},  # energy sold to the grid
            '/Ac/L1/Energy/Forward': {'initial': 0},
            '/Ac/L1/Energy/Reverse': {'initial': 0},  # energy sold to the grid
            PATH_UPDATEINDEX: {'initial': 0},
        })

    logging.info(
        'Connected to dbus, and switching over to gobject.MainLoop() (= event based)')
    mainloop = gobject.MainLoop()
    mainloop.run()


# Configuration MQTT
client = mqtt.Client(config['DEFAULT']['device_name'])  # create new instance
client.on_disconnect = on_disconnect
client.on_connect = on_connect
client.on_message = on_message
client.connect(config['MQTT']['broker_address'])  # connect to broker

client.loop_start()

if __name__ == "__main__":
    main()
