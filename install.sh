#!/bin/bash
plugin_name='em3-mqtt-meter'
# set permissions for script files
chmod a+x /data/$plugin_name/restart.sh
chmod 744 /data/$plugin_name/restart.sh

chmod a+x /data/$plugin_name/uninstall.sh
chmod 744 /data/$plugin_name/uninstall.sh

chmod a+x /data/$plugin_name/service/run
chmod 755 /data/$plugin_name/service/run



# create sym-link to run script in deamon
ln -s /data/$plugin_name/service /service/$plugin_name



# add install-script to rc.local to be ready for firmware update
filename=/data/rc.local
if [ ! -f $filename ]
then
    touch $filename
    chmod 755 $filename
    echo "#!/bin/bash" >> $filename
    echo >> $filename
fi

grep -qxF "/data/$plugin_name/install.sh" $filename || echo "/data/$plugin_name/install.sh" >> $filename
